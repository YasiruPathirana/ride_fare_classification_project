import numpy as np
from sklearn.ensemble import RandomForestClassifier
from Preprocessing import *


def random_forest(x_train, x_test, tripid, y_train, sample_data):
    model = RandomForestClassifier()
    model.fit(x_train, y_train)

    y_pred = model.predict(x_test)

    sample_data['tripid'] = tripid
    sample_data['prediction'] = y_pred

    write_result_to_file(sample_data)


def create_model():
    train_data_set = read_data_set('RideFareDataFiles//train.csv', True)
    test_data_set = read_data_set('RideFareDataFiles//test.csv', False)
    sample_data_set = read_file('RideFareDataFiles//results.csv')

    x_train, x_test, tripid, y_train = preProcessData(train_data_set, test_data_set)

    random_forest(x_train, x_test, tripid, y_train, sample_data_set)


create_model()
