from Preprocessing import *
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import ExtraTreesClassifier

"""
    Checking Pearson Correlation between features to remove similar features
"""


def correlaction_check(dataFrame):
    # get correlations of each features in dataset
    corrmatrix = dataFrame.corr()
    top_corr_features = corrmatrix.index
    plt.figure(figsize=(20, 20))
    # plot heat map
    g = sns.heatmap(dataFrame[top_corr_features].corr(), annot=True, cmap="RdYlGn")


"""
 Use SelectKBest Method to identify best features for training
"""


def selectKBest(X, y):
    bestfeatures = SelectKBest(score_func=chi2, k=10)
    fit = bestfeatures.fit(X, y)
    dfscores = pd.DataFrame(fit.scores_)
    dfcolumns = pd.DataFrame(X.columns)
    # concat two dataframes for better visualization
    featureScores = pd.concat([dfcolumns, dfscores], axis=1)
    featureScores.columns = ['Specs', 'Score']  # naming the dataframe columns
    print(featureScores.nlargest(12, 'Score'))  # print 10 best features


"""
 Use Extra Tree Classifier to choose important features in ascending order
"""


def feature_importance(X, y):
    model = ExtraTreesClassifier()
    model.fit(X, y)
    print(model.feature_importances_)  # use inbuilt class feature_importances of tree based classifiers
    # plot graph of feature importances for better visualization
    feat_importances = pd.Series(model.feature_importances_, index=X.columns)
    feat_importances.nlargest(5).plot(kind='barh')
    plt.show()

def create_model():
    train_data_set = read_data_set('RideFareDataFiles//train.csv', True)
    test_data_set = read_data_set('RideFareDataFiles//test.csv', False)

    x_train, x_test, tripid, y_train = preProcessData(train_data_set, test_data_set)

    selectKBest(x_train, y_train)
    feature_importance(x_train, y_train)
    correlaction_check(x_train)


create_model()
