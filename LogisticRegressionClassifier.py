
from Preprocessing import *
from sklearn.linear_model import LogisticRegression


def logistic_regression(x_train, x_test, tripid, y_train, sample_data):

    LogRegModel = LogisticRegression(C=100, random_state=42, solver='liblinear', max_iter=500)
    LogRegModel.fit(x_train, y_train)

    y_pred = LogRegModel.predict(x_test)

    sample_data['tripid'] = tripid
    sample_data['prediction'] = y_pred

    write_result_to_file(sample_data)


def create_model():
    train_data_set = read_data_set('RideFareDataFiles//train.csv', True)
    test_data_set = read_data_set('RideFareDataFiles//test.csv', False)
    sample_data_set = read_file('RideFareDataFiles//results.csv')

    x_train, x_test, tripid, y_train = preProcessData(train_data_set, test_data_set)

    logistic_regression(x_train, x_test, tripid, y_train, sample_data_set)


create_model()