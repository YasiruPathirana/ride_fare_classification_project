import time
import pandas as pd
import numpy as np

""" Read and Preprocess Data Set"""


def read_data_set(file_name, bIsTrain):
    if bIsTrain:
        dataFrame = pd.read_csv(file_name,
                                usecols=['duration',
                                         'additional_fare',
                                         'meter_waiting',
                                         'meter_waiting_fare',
                                         'meter_waiting_till_pickup',
                                         'fare',
                                         'label',
                                         'pickup_time',
                                         'drop_time',
                                         'pick_lat',
                                         'pick_lon',
                                         'drop_lat',
                                         'drop_lon'
                                         ])

        dataFrame.label = [1 if each == 'correct' else 0 for each in dataFrame.label]

    else:
        dataFrame = pd.read_csv(file_name,
                                usecols=['tripid',
                                         'duration',
                                         'additional_fare',
                                         'meter_waiting',
                                         'meter_waiting_fare',
                                         'meter_waiting_till_pickup',
                                         'fare',
                                         'pickup_time',
                                         'drop_time',
                                         'pick_lat',
                                         'pick_lon',
                                         'drop_lat',
                                         'drop_lon'
                                         ])

    pattern = '%m/%d/%Y %H:%M'
    if 'pickup_time' in dataFrame:
        dataFrame.pickup_time = [int(time.mktime(time.strptime(tt1, pattern))) for tt1 in dataFrame.pickup_time]
    if 'drop_time' in dataFrame:
        dataFrame.drop_time = [int(time.mktime(time.strptime(tt2, pattern))) for tt2 in dataFrame.drop_time]

    return dataFrame


""" Read a CSV file using pandas """


def read_file(filename):
    return pd.read_csv(filename)


"""Write the Results to CSV File"""


def write_result_to_file(data_set):
    data_set.to_csv('RideFareDataFiles//Results.csv', index=False)


def preProcessData(train_data, test_data):
    # Trip ID in test set saved
    tripid = test_data['tripid']

    # Label of the testing set
    y_train = train_data['label']
    x_train = train_data
    x_train = x_train.drop(['label'], axis=1)
    x_train = x_train.replace(to_replace=np.nan, value=0)

    x_test = test_data
    x_test = x_test.drop(['tripid'], axis=1)
    x_test = x_test.replace(to_replace=np.nan, value=0)

    return x_train, x_test, tripid, y_train
