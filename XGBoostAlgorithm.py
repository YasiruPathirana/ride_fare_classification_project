from Preprocessing import *
import xgboost as xgb


def XGBoostAlgorithm(x_train, x_test, tripid, y_train, sample_data):
    model_XGBClassifier = xgb.XGBClassifier(learning_rate=0.1, objective='reg:logistic',
                                            colsample_bytree=0.3, n_estimators=10, max_depth=5)
    model_XGBClassifier.fit(x_train, y_train)

    y_pred = model_XGBClassifier.predict(x_test)

    sample_data['tripid'] = tripid
    sample_data['prediction'] = y_pred

    write_result_to_file(sample_data)


def create_model():
    train_data_set = read_data_set('RideFareDataFiles//train.csv', True)
    test_data_set = read_data_set('RideFareDataFiles//test.csv', False)
    sample_data_set = read_file('RideFareDataFiles//results.csv')

    x_train, x_test, tripid, y_train = preProcessData(train_data_set, test_data_set)

    XGBoostAlgorithm(x_train, x_test, tripid, y_train, sample_data_set)


create_model()
